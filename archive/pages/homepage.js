import React from 'react';
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { Helmet } from 'react-helmet';

const StyledPage = styled.body`
  width: 100%;
`

const StyledLeft = styled.body`
  width: 50%;
  position: fixed;
  background-color: white;
  color: black;
`

const StyledRight = styled.body`
  width: 50%;
  margin-left: 50%;
  min-height: 100vh;
  position: fixed;
  background-color: black;
  color: white;
`

const StyledText = styled.p`
  font-family: 'Open Sans', sans-serif;
  font-size: 3.5vw;
  margin-left: 25%;
  margin-right: 25%;
  margin-top: 40vh;
  margin-bottom: 50vh;
  text-align: center;

  .react-rotating-text-cursor {
    animation: blinking-cursor 0.8s cubic-bezier(0.68, 0.01, 0.01, 0.99) 0s infinite;
  }
   
  @keyframes blinking-cursor {
    0% {
      opacity: 0;
    }
    50% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }

  @media (max-width: 479px) {
    font-size: 5vw;
    margin-left: 20%;
    margin-right: 25%;
  }
`

const StyledLeftNavLink = styled(NavLink)`
  text-decoration: none;
  color: black;
  &:visited {
    color: black;
  }
  &:hover {
    text-decoration: underline;
  }
`

const StyledRightNavLink = styled(NavLink)`
  text-decoration: none;
  color: white;
  &:visited {
    color: white;
  }
  &:hover {
    text-decoration: underline;
  }
`

const Homepage = () => {
  return (
    <StyledPage>
      <Helmet>
        <title>Navin Mani</title>
      </Helmet>
      <StyledLeft>
        <StyledText>
          <StyledLeftNavLink to='/internal'>
            Navin
          </StyledLeftNavLink>
        </StyledText>
      </StyledLeft>
      <StyledRight>
        <StyledText>
          <StyledRightNavLink to='/external'>
            Money
          </StyledRightNavLink>
        </StyledText>
      </StyledRight>
    </StyledPage>
  );
}

export default Homepage;