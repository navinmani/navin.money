import React from 'react';
import styled from "styled-components";
import { NavLink } from "react-router-dom";

const StyledFooter = styled.nav`
  padding-left: 15%;
  padding-bottom: 3%;
  background-color: ${props => props.tao === 'yin' ? 'black' : 'white'}; 

  @media (max-width: 479px) {
    font-size: 17px;
    padding-right: 8%;
    padding-bottom: 5%;
  }
`

const StyledWebLink = styled(NavLink)`
  font-size: 17px;
  font-family:'Open Sans', sans-serif;
  text-decoration: none;
  font-weight: 600;
  padding-right: 1%;
  color: ${props => props.tao === 'yin' ? 'white' : 'black'};
  &:hover {
    text-decoration: underline;
  }

  @media (max-width: 479px) {
    font-size: 12px;
    padding-right: 3%;
  }
`

const MeditationFooter = (props) => {
  return (
    <StyledFooter tao={props.tao}>
      <StyledWebLink to="/redirect-to-instagram" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        Instagram
      </StyledWebLink>
      <StyledWebLink to="/redirect-to-twitter" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        Twitter
      </StyledWebLink>
    </StyledFooter>
  );
}

const TechnologyFooter = (props) => {
  return (
    <StyledFooter tao={props.tao}>
      <StyledWebLink to="/redirect-to-gitlab" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        GitLab
      </StyledWebLink>
      <StyledWebLink to="/redirect-to-twitter" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        Twitter
      </StyledWebLink>
      <StyledWebLink to="/redirect-to-linkedin" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        LinkedIn
      </StyledWebLink>
      <StyledWebLink to="/redirect-to-dev" target="_blank" rel="noopener noreferrer" tao={props.tao}>
        DEV
      </StyledWebLink>
    </StyledFooter>
  );
}

export default { MeditationFooter, TechnologyFooter };