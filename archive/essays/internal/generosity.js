import React from 'react';
import styled from "styled-components";
import { Helmet } from 'react-helmet';

const StyledPage = styled.div`
  margin-bottom: 2%;
  font-family: 'Amiri', serif;
`

const StyledDate = styled.p`
  font-size: 18px;
  line-height: 23px;

  @media (max-width: 479px) {
    font-size: 14px;
    line-height: 18px;
  }
`

const StyledEssay = styled.p`
  font-size: 30px;
  line-height: 42px;
  padding-left: 7%;

  a {
    color: black;
  }

  @media (max-width: 850px) {
    font-size: 25px;
    line-height: 32px;
    padding-left: 0px;
  }

  @media (max-width: 479px) {
    font-size: 22px;
    line-height: 24px;
    padding-left: 0px;
  }
`

const Generosity = () => {
  return (
    <StyledPage>
      <Helmet>
        <title>Generosity</title>
      </Helmet>
      <StyledEssay>
        According to Buddhist meditative practices, the first step towards true understanding is dana, or generosity.
        Those that practice generosity, and put stinginess to rest, will be able to reach a valuable level of joy and peace.
        Additionally, they will be able to continue towards the next level of understanding, virtue.
        <br/>
        As of the past month, my emotions have tensed and I have been experiencing frustration, agitation, and occasionally, rage.
        I could spend plenty of time rationalizing out why (quarantine, constriction, lack of socialization, etc.), but I realize that
        practicing generosity is how I can overcome these negative emotions.
        <br/>
        This practice comes in two forms, for me. The first is metta, or loving-kindness meditation.
        In addition to a regular breath meditation practice, a daily practice of metta will help me open up my gratitude, care, and love for others.
        Secondly, by actively being generous and serving those around me, I can grow and cultivate this love.
        <br/>
        Today is the second day of daily metta. Today I voluntarily helped my parents pack, and chose to run some errands for them.
        <StyledDate>June 2020</StyledDate>
      </StyledEssay>
    </StyledPage>
  );
}

export default Generosity;