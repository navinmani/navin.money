import React from 'react';
import styled from "styled-components";
import { Helmet } from 'react-helmet';

const StyledPage = styled.div`
  margin-bottom: 2%;
  font-family: 'Amiri', serif;
`

const StyledDate = styled.p`
  font-size: 18px;
  line-height: 23px;

  @media (max-width: 479px) {
    font-size: 14px;
    line-height: 18px;
  }
`

const StyledEssay = styled.p`
  font-size: 30px;
  line-height: 42px;
  padding-left: 7%;

  a {
    color: black;
  }

  @media (max-width: 850px) {
    font-size: 25px;
    line-height: 32px;
    padding-left: 0px;
  }

  @media (max-width: 479px) {
    font-size: 22px;
    line-height: 24px;
    padding-left: 0px;
  }
`

const Dreams = () => {
  return (
    <StyledPage>
      <Helmet>
        <title>Dreams</title>
      </Helmet>
      <StyledEssay>
        My past few months have been fairly stressful. I haven't been too strained by it, but I have a lot on my plate at the moment.
        I'm planning for the future, taking classes, and working on projects.
        I had not realized how these large desires (to set up a good future, to ace my classes, to succeed in these projects)
        had been taking a toll on my mindstate and my happiness.
        <br/>
        I knew that taking on all of these responsibilities would be difficult, but I also knew that I could handle it. And I was right.
        If I want to accomplish all of these things, I can. However, to do so I have been sacrificing my peace of mind.
        <br/>
        For the past few weeks, instead of dreaming I had been planning. When I woke up, I would feel the remnants of my daily desires seeping into the night. For example, 
        some days I would wake up thinking about a strategy for graduate school (because while sleeping, I was strategizing about graduate school). Or my mind
        would grind through the night, thinking about which company I should try to get a job at. Again, these desires haven't caused any extra headaches and I have still
        been able to set time aside for pleasures like reading and playing the piano. But recently, I've realized that they have been clouding my peace.
        <br/>
        A few days ago I made the decision to have one large desire at a time (shoutout Naval Ravikant). This means that I've funneled my active efforts into one desire.
        I put aside time to tend to the other desires, but this focus has allowed me to be mentally impacted by only one desire. When my mind wanders, it wanders to 
        one place now, instead of any of three (or more).
        <br/>
        Last night, I had one long dream that lasted throughout the night. In the dream, I lived through many days, and there was one long narrative.
        The dream was crystal clear, my motivations were apparent, and I was focused on the one dream. I woke up feeling mentally clear, and I sat with a level
        of peace I have not experienced in the past few months.
        <StyledDate>Oct 2020</StyledDate>
      </StyledEssay>
    </StyledPage>
  );
}

export default Dreams;