import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

const StyledPage = styled.div`
  margin-bottom: 2%;
  font-family: 'Amiri', serif;
`

const StyledDate = styled.p`
  font-size: 18px;
  line-height: 23px;

  @media (max-width: 479px) {
    font-size: 14px;
    line-height: 18px;
  }
`

const StyledEssay = styled.p`
  font-size: 30px;
  line-height: 42px;
  padding-left: 7%;

  a {
    color: white;
  }

  @media (max-width: 850px) {
    font-size: 25px;
    line-height: 32px;
    padding-left: 0px;
  }

  @media (max-width: 479px) {
    font-size: 22px;
    line-height: 24px;
    padding-left: 0px;
  }
`

const Azimuth = () => {
  return (
    <StyledPage>
      <Helmet>
        <title>Azimuth</title>
      </Helmet>
      <StyledEssay>
        <a href="https://azimuth.to/" target="_blank" rel="noopener noreferrer">
          Right now, I am helping people find independence away from their phone.
        </a> Many people struggle with phone addiction. More people than that use their phones too often, and with guilt.
        At Azimuth, we empower people to take control of their routines, and to do the things they care about.
        <StyledDate>July 2020</StyledDate>
      </StyledEssay>
    </StyledPage>
  );
}

export default Azimuth;