import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

const StyledPage = styled.div`
  margin-bottom: 2%;
  font-family: 'Amiri', serif;
`

const StyledDate = styled.p`
  font-size: 18px;
  line-height: 23px;

  @media (max-width: 479px) {
    font-size: 14px;
    line-height: 18px;
  }
`

const StyledEssay = styled.p`
  font-size: 30px;
  line-height: 42px;
  padding-left: 7%;

  a {
    color: white;
  }

  @media (max-width: 850px) {
    font-size: 25px;
    line-height: 32px;
    padding-left: 0px;
  }

  @media (max-width: 479px) {
    font-size: 22px;
    line-height: 24px;
    padding-left: 0px;
  }
`

const Neuroscience = () => {
  return (
    <StyledPage>
      <Helmet>
        <title>Innovations in Neuroscience</title>
      </Helmet>
      <StyledEssay>
        The brain is the most complex organ in the body. We are able to relate to the world only through the sensations and thoughts 
        that arise in the brain. The brain tells us, or rather decides, when good things are happening and when bad things are happening.
        Everything from subconscious decision making to rational thought to basal desires, everything that we do ever, is directly
        associated with the brain. This complexity elevates the importance of neuroscience; the brain is one of the most important 
        things humans should be aiming to gain mastery of right now. This complexity also means that it is super f***ing hard to 
        understand the brain. Millennia of curiosity about the brain has taken us far, but not far enough. Modern science still 
        lacks a complete understanding of how one’s neurology impacts one’s experience for the better or worse. Yes, we have a really
        good understanding, but not good enough to solve many neurocognitive and neurodegenerative diseases. Diseases such as Major 
        Depressive Disorder, Alzheimer’s Disease, and Huntington’s Disease are just three of the dozens of ailments that are considered
        the most horrendous and harmful of all disorders and they plague millions and kill hundreds of thousands annually. We still have
        a lot of work to do. But, as you know, technology moves FAST. Painting a cohesive, exhaustive, and detailed mosaic of neurology 
        has been extremely difficult, but now we are more capable than ever to do so. Innovations in psychology, software, and hardware 
        are making neuroscientific breakthroughs easier to discover and create. Sometime soon, these innovations can help you understand 
        your brain and live a better, more beautiful life.
        <br/>
        Psychological advancements are fundamental to the understanding of neurological diseases. In 2008, the US National Institute of 
        Mental Health (NIMH) began the <a href='https://www.nimh.nih.gov/research/research-funded-by-nimh/rdoc/constructs/rdoc-matrix.shtml' 
        target="_blank" rel="noopener noreferrer">Research Domain Criteria (RDoC)</a> project, wherein they “<a href='https://en.m.wikipedia.org/wiki/Research_Domain_Criteria' 
        target="_blank" rel="noopener noreferrer">attempt to create a new kind of taxonomy 
        for mental disorders by bringing the power of modern research approaches in genetics, neuroscience, and behavioral science to 
        the problem of mental illness.</a>” This is a huge development for a field that has historically relied on a doctrine, the Diagnostic
        and Statistical Manual of Mental Disorders (DSM), that is based in consensual opinion rather than objective measures. This 
        doctrine, while widely used, limited the rate of innovation within the field; it was a weak cornerstone on which much of today’s
        psychological practice was built. This important step taken by the NIMH is helping pull the study of psychology from <a href='https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4629069/' 
        target="_blank" rel="noopener noreferrer">largely theoretical to largely scientific</a>, a step that directly destigmatizes 
        <a href='https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2080544/' target="_blank" rel="noopener noreferrer"> our cultural view of mental illnesses.</a> In effect, this
        project is remixing the mortar and relaying the brick on which informed, accurate, and impactful psychological innovations 
        can be made.
        <br/>
        The advent of new machine learning methods and the availability of computer power has expanded the breadth of situations for
        which machine learning can be used effectively. In essence, where there are ample data, there is the opportunity for data
        analysis. <a href='https://en.m.wikipedia.org/wiki/Neuroimaging' target="_blank" rel="noopener noreferrer">The use of neuroimaging</a> dates back to 1927,
        and today we are using methods that were introduced in the 1970’s and
        1980’s such as CT and MRI. So in our neuroscientific context, there are ample neuroimaging data, and machine learning can find
        patterns in these images very well. With decades of data, we can do amazing things like <a href='https://pubmed.ncbi.nlm.nih.gov/30398430/'
         target="_blank" rel="noopener noreferrer">train machine learning models on PET scan images to predict Alzheimer’s Disease over 6 years before a final diagnosis.</a> This
        is also why this Fall 2020, I am beginning
        a project wherein I am training machine learning models to analyze EEG and PET scans of Major Depressive Disorder patients 
        (reach out if you want to contribute!). When the right machine learning models are trained over good data, doctors will be 
        able to diagnose and treat many neurological disorders with higher accuracy and care. As we gather more and more neuroimages,
        and create new tools to gather novel data or do something traditional more accurately, our models will enable us to understand 
        the complexities that are inherent in the brain.
        <br/>
        Finally, I want to highlight <a href='https://www.mitpressjournals.org/doi/full/10.1162/netn_a_00103' target="_blank" rel="noopener noreferrer">
        the emerging field of clinical network neuroscience</a>, or connectomics. This field is home to a number
        of neuroscientists who share the goal of fully mapping out the insanely complex molecular network structure of the brain. Building
        a network of this caliber requires huge amounts of compute power and <a href='https://www.anl.gov/profile/narayanan-kasthuri' target="_blank" rel="noopener noreferrer">
        novel technologies</a> that allow us to see the brain at a high
        resolution, two things that could very well be available within the next 5 years. By building a molecular mapping of the brain 
        we can deeply understand neurobiological structure and how it relates to neurological function. From there, we could virtually 
        simulate any number of events on a particular brain, meaning we could literally conduct neurological experiments without requiring
        an actual brain. This would remove the mammoth obstacle of needing live human brains to conduct these experiments and accelerate
        the rate at which we discover innovations in the field.
        <br/>
        Technology moves really fast, which means that these approaching neuroscientific innovations will certainly have a big impact on
        your life. Right now, scientists all over the world are focusing their efforts in building brain-machine interfaces that allow 
        us to communicate with our brains. <a href='https://www.neuralink.com/' target="_blank" rel="noopener noreferrer">Neuralink</a> is
        perhaps the closest to getting <a href='https://www.biorxiv.org/content/10.1101/703801v4.full' target="_blank" rel="noopener noreferrer">one of these</a> in your hands, or rather in your 
        brain, tomorrow. Last week they demoed their chip in healthy pigs, and proved that their 23mm by 8mm device can accurately 
        transmit brain signals outwardly, and that the implant has essentially no negative side effects. Their goal is to make these 
        chips affordable and safe, such that “anyone who wants one can have one.” First, they plan to use the chip to help solve 
        neurological disorders such as paraplegia and memory loss. Over time, this chip will be available for consumer purchase, and can
        be implanted in under an hour without the need for general anesthesia. I am impartial whether you should go sign up to get a 
        Neuralink implant whenever they are released to the public. I bring this example up because I want to shed light on the
        possibilities that exist around neuroscientific innovations, and how the field is getting increasingly closer to comprehensively
        understanding everything the brain does and how it does it. This understanding will be a feat for the history books, and will be
        looked back as a turning point that changed the course of technological progression and human potential forever. Soon, we will be
        able to solve some of humanity’s most devastating neurological diseases. Soon, we will be able to communicate with our brain to 
        fundamentally change our experience of the world.
        <StyledDate>August 2020</StyledDate>
      </StyledEssay>
    </StyledPage>
  );
}

export default Neuroscience;